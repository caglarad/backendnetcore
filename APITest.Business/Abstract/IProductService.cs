﻿using APITest.Core.Utilities.Results;
using APITest.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APITest.Business.Abstract
{
    public interface IProductService
    {
        IDataResult<IQueryable<Product>> GetAll();
        IDataResult<Product> GetById(int id);
        IResult ProductExist();
        IResult Add(Product);
    }
}
