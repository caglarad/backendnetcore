﻿using APITest.Business.Abstract;
using APITest.Business.ValidationRules.FluentValidation;
using APITest.Core.CrossCuttingConcern.Validation;
using APITest.Core.Utilities.Results;
using APITest.DataAccess.Abstract;
using APITest.Entities.Concrete;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APITest.Business.Concrete
{
    public class ProductManager : IProductService
    {
        private readonly IProductDal _productDal;
        public ProductManager(IProductDal productDal)
        {
            _productDal = productDal;
        }

        public IResult Add(Product product)
        {
            ValidationTool.Validate(new ProductValidator(), product);

            _productDal.Add(product);
            return new Result("Ürün ekleme işlemi başarılı", true);
        }

        IDataResult<IQueryable<Product>> IProductService.GetAll()
        {
            return new SuccessDataResult<IQueryable<Product>>(_productDal.GetAll(),"Ürünler Listelendi");
        }

        IDataResult<Product> IProductService.GetById(int id)
        {
            return new SuccessDataResult<Product>(_productDal.Get(x => x.ProductID == id), "Ürün Listelendi");
        }

        IResult IProductService.ProductExist()
        {
            throw new NotImplementedException();
        }
    }
}
