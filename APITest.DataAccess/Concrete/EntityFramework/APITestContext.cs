﻿using APITest.Entities.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APITest.DataAccess.Concrete.EntityFramework
{
    public class APITestContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-11VROCF\SQLEXPRESS;Database=NORTHWND;Trusted_Connection=true");
        }
        public DbSet<Product> Products { get; set; }
    }
}
