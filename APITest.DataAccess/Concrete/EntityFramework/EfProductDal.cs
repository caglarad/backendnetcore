﻿using APITest.Core.DataAccess.EntityFramework;
using APITest.DataAccess.Abstract;
using APITest.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace APITest.DataAccess.Concrete.EntityFramework
{
    public class EfProductDal :EfEntityRepositoryBase<Product,APITestContext>, IProductDal
    {
        
    }
}
